# coding: utf-8
'''
    File name: csv_from_excel.py
    Author: Mônica Ulguim Nunes
    Created at: 02/14/2019
    Last modified at: 02/15/2019
    Python Versions: 3.6
'''
import csv
import openpyxl

def csv_from_excel():
    file = input("File path: \n")
    wb = openpyxl.load_workbook(file, data_only=True)
    filename = file.rsplit(".", 1)[0]
    sheet_count = 0
    for sheet in wb.worksheets:        
        csv_file = open(filename + "_" + wb.sheetnames[sheet_count] + '.csv', 'w', encoding='utf-8')
        writer = csv.writer(csv_file, quoting=csv.QUOTE_ALL, delimiter=';', lineterminator='\n')
        for r in sheet.rows:
            row = [str(cell.value).replace('"', '\'').replace('None', '').strip() for cell in r]
            if not blank_row(row):
                writer.writerow(row)
        sheet_count += 1

def blank_row(row):
    for value in row:
        if value != "" and not str(value).isspace() and value != "None":
            return False
    return True

csv_from_excel()
