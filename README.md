# CSV_FROM_EXCEL

Convert MS Excel files (xls, xlsm, ...) to cross-platform .csv files 



 ## get started

clone this repository

git clone https://bitbucket.org/monicaununes/csv_from_excel.git

install requirements 

pip install -r requirements.txt

run

python csv_from_excel.py

type full file path and wait for the magic

shell
output:

File path:
C:\test\mysheets.xlsm



## support 

Windows (Linux support coming soon)

Python 3.6.x